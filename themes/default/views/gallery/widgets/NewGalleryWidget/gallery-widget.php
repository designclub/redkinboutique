    <div class="brend-slider">
        <?php foreach ($models->images as $key => $item): ?>
            <div class="brend-slider__item">
                <div class="brend-slider-item__img" style="position: relative">
                    <?= CHtml::image($item->getImageUrl(550,354,true), "Одежда из Турции
                    по всей России и СНГ"); ?>
                    <div class="rd" style="position: absolute">
                        <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/rd.png') ?>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
    </div>
    <div class="brend-slider-nav-wrap">
        <div class="brend-slider-nav">
            <?php foreach ($models->images as $key => $item): ?>
                <div class="brend-slider-nav__item">
                    <?= CHtml::image($item->getImageUrl(200,115,true), "Одежда из Турции
                    по всей России и СНГ"); ?>
                </div>
            <?php endforeach ?>
        </div>
  </div>
