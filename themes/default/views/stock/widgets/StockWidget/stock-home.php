<?php if($models) : ?>
	<div class="stock-box stock-box-carousel">
		<?php foreach ($models as $key => $data) : ?>
			<div class="stock-box__item">
				<div class="stock-box__img">
					<?= CHtml::image($data->getImageUrl(), ''); ?>
				</div>
				<div class="stock-box__info">
					<div class="stock-box__name-before">
						<?= $data->name_before; ?>
					</div>
					<div class="stock-box__name">
						<?= $data->name_short; ?>
					</div>
					<div class="stock-box__desc">
						<?= $data->description_short; ?>
					</div>
					<?php if($data->button_name) : ?>
						<div class="stock-box__but">
							<a class="stock-box__link but but-green" href="<?= $data->button_link; ?>"><?= $data->button_name; ?></a>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>