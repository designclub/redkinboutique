<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_START);?>
     <?php
      Yii::app()->clientScript->registerMetaTag('index, follow', 'robots');
      ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="Content-Language" content="ru-RU" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" type="image/ico" href="https://redkinboutique.ru/favicon.ico" />

    <meta name="yandex-verification" content="03d33f05ed93a8b2" />
    <title><?= $this->title;?></title>
    <meta name="description" content="<?= $this->description;?>" />
    <meta name="keywords" content="<?= $this->keywords;?>" />

    <?php if ($this->canonical) : ?>
        <link rel="canonical" href="<?= $this->canonical ?>" />
    <?php endif; ?>

    <?php 
    Yii::app()->getClientScript()->registerLinkTag('stylesheet','text/css',$this->mainAssets . '/css/style.min.css','all');
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/scripts.min.js',CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/store.js',CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile('https://www.google.com/recaptcha/api.js',CClientScript::POS_END);
    ?>
    <script type="text/javascript">
        var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
    </script>
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- <link rel="stylesheet" href="http://yandex.st/highlightjs/8.2/styles/github.min.css">
    <script src="http://yastatic.net/highlightjs/8.2/highlight.min.js"></script> -->
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_END);?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-159164507-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-159164507-1');
        </script>
</head>

<body>

<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_START);?>


<div class='wrapper'>
    <!-- flashMessages -->
    <?php //$this->widget('yupe\widgets\YFlashMessages'); ?>

    <?php $this->renderPartial('//layouts/_header'); ?>
    <div class="content">
        <?php if (!$this->main_page): ?>
         <div class="container breadcrumbs_wrap">
      <?php $this->widget(
        'bootstrap.widgets.TbBreadcrumbs',
        [
            'links' => $this->breadcrumbs,
        ]
    );?>
    </div>
     <?php endif ?>
        <?= $this->decodeWidgets($content); ?>
    </div>
    <!-- footer -->
    <?php $this->renderPartial('//layouts/_footer'); ?>
    <!-- footer end -->
</div>

<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_END);?>

<?php $this->widget('application.modules.mail.widgets.WriteToUsWidget'); ?>
<?php $this->widget('application.modules.mail.widgets.CallbackWidget',['view' => 'callback-widget']); ?>
<div id="messageModal" class="modal modal-my fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div data-dismiss="modal" class="modal-close"><div></div></div>
                <div class="modal-header__heading">
                    <div class="modal-header__h2">Уведомление</div>
                </div>
            </div>
            <div class="modal-body">
                <div class="message-success">
                    Ваша заявка успешно отправлена!
                </div>
            </div>
        </div>
    </div>
</div>
<div class='notifications top-right' id="notifications"></div>
<div class="ajax-loading"></div>

</body>
</html>
