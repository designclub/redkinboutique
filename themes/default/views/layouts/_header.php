<header>
    <div class="container d-flex top-header">
        <div class="top-header__first">
            <a class="first__instagram" href="https://www.instagram.com/redkin_boutique/" target="_blank">
                <i></i>
                <span>Интернет-магазин детской одежды Redkin baby boutique</span>
            </a>
            <div class="first__search">
                <?php $this->widget('application.modules.store.widgets.SearchProductWidget'); ?>
            </div>
            <div class="mobile-menu" id="mobile-menu">
             <div class="mobile-menu__name">
                 Меню
             </div>
             <div class="toggler">
               <span></span>
             </div>
        </div>
        </div><!--top-header__first-->

        <a class="top-header__logo" href="/">

            <?= CHtml::image($this->mainAssets . '/images/logo.jpg','Интернет-магазин детской одежды Redkin baby boutique') ?>

        </a>
        <div class="top-header__three">
            <div class="reqweesits">
                <a class="reqweesits__mail" href="#">
                    <i></i>
                    <span>redkinboutique@yandex.ru</span>
                </a>
                <a class="reqweesits__phone" href="tel:<?= Yii::app()->getModule('yupe')->p_reception; ?>">
                    <i></i>
                    <span><?= Yii::app()->getModule('yupe')->reception; ?></span>
                </a>
            </div>
            <div class="widgets-buttons">
                <div class="user">
                    <?php if (Yii::app()->user->isGuest): ?>
                        <a class="" href="<?= Yii::app()->createUrl('user/account/login'); ?>">
                            <i class="icon"></i>
                            <span>Войти</span>
                        </a>
                    <?php else: ?>
                        <a class="" href="<?= Yii::app()->createUrl('user/profile/index'); ?>">
                            <i class="icon"></i>
                            <span>Личный кабинет</span>
                        </a>
                    <?php endif ?>
                </div>

                <div class="favorits">
                    <?php if(Yii::app()->hasModule('favorite')):?>

                            <a class="toolbar-button" href="<?= Yii::app()->createUrl('/favorite/default/index'); ?>">

                                    <div class="but-favorite__icon but-header__icon">
                                        <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/heart.svg'); ?>
                                        <span class="but-favorite__count but-header__count <?= (Yii::app()->favorite->count() != null) ? 'active' : ''; ?>" id="yupe-store-favorite-total"><?= Yii::app()->favorite->count();?></span>
                                    </div>
                                    <div class="but-favorite__icon__text but-header__text">Избранное</div>
                                </a>


                    <?php endif;?>
                </div>
                <div class="cart">
                    <?php if (Yii::app()->hasModule('cart')): ?>
                        <div class="header-cart shopping-cart-widget" id="shopping-cart-widget">
                            <?php $this->widget('application.modules.cart.widgets.ShoppingCartWidget'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div><!--top-header__three-->
    </div>
</header>
<div class="menu" id="hideOnScroll">
        <div class="container d-flex">
                <?php $this->widget('application.modules.store.widgets.CategoryWidget', ['depth' => 1]); ?>
                <?php if (Yii::app()->hasModule('menu')): ?>
                   <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu']); ?>
                <?php endif; ?>
        </div>
  </div><!-- menu -->
  <div class="menu-adaptive" id="menu-adaptive">
    <div class="container container-menu d-flex">
          <div class="menu-adaptive__catalog">
              <?php $this->widget('application.modules.store.widgets.CategoryWidget', ['depth' => 1,'view'=>'category-footer']); ?>
            </div>
            <div class="menu-adaptive__main-menu">
                <div class="main-menu__item-name">Информация</div>
              <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu']); ?>
            </div>
        </div>
  </div>

<?php $this->widget('application.modules.store.widgets.SearchProductWidget', ['view' => 'search-product-form-modal']); ?>