<footer>
    <div class="container d-flex">
        <div class="footer__item footer__item-catalog">
          <?php $this->widget('application.modules.store.widgets.CategoryWidget', ['depth' => 1,'view'=>'category-footer']); ?>
        </div>
        <div class="footer__item">
            <div class="footer__item-name">Информация</div>
          <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu']); ?>
        </div>
        <div class="footer__item contacts_item">
            <div class="footer__item-name">Контакты</div>
                <a href="tel:<?= Yii::app()->getModule('yupe')->p_reception; ?>" class="footer-phone__link">
                    <i></i>
                    <span><?= Yii::app()->getModule('yupe')->reception; ?></span>
                </a>
                <a href="#" class="footer-mail__link">
                    <i></i>
                    <span>redkinboutique@yandex.ru</span>
                </a>
                <a class="footer-social__link" href="https://www.instagram.com/redkin_boutique/" target="_blank">
                    <i></i>
                    <span>Мы в соцсетях</span>
               </a>
               <a href="#" class="but but-black" data-toggle="modal" data-target="#writetousModal">Написать нам</a>
        </div>
    </div>
</footer>
<div class="footer-bottom">
    <div class="container d-flex">
        <a href="/" class="redkin">
            <div class="redkin-img">
                <?= CHtml::image($this->mainAssets . '/images/rl.jpg','REDKIN') ?>
            </div>
            <div class="redkin-desc">
                Интернет-магазин детской одежды <br>
                Redkin baby boutique
            </div>
        </a>
        <a href="https://designclub56.ru/" class="dc">
            <div class="dc-img">
                <?= CHtml::image($this->mainAssets . '/images/dc.png','DCMedia        ') ?>
            </div>
            <div class="dc-desc">
                Создание и продвижение сайтов
            </div>
        </a>
    </div>
</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
   ym(57826462, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/57826462" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->