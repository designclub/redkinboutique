<?php
/** @var Page $page */

if ($page->layout) {
    $this->layout = "//layouts/{$page->layout}";
}
$this->main_page = true;
$this->promo = $page->promo;
$this->title = Yii::app()->getModule('yupe')->siteName;
$this->description = !empty($page->meta_description) ? $page->meta_description : Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = !empty($page->meta_keywords) ? $page->meta_keywords : Yii::app()->getModule('yupe')->siteKeyWords
?>
<div class="container">
<?php $this->widget('application.modules.slider.widgets.SliderWidget'); ?>
</div>
<div class="container">
    <?php $this->widget('application.modules.store.widgets.CatalogWidget',['view' => 'view-home']); ?>
</div>
<div class="container container-pr">
    <div class="brend-image">
        <div class="brend-image__img">
            <?= CHtml::image($this->mainAssets . '/images/red.png','REDKIN') ?>
        </div>
    </div>
    <?php $this->widget('application.modules.store.widgets.ProductTypeWidget', [
    'condition' => 'is_popular = 1',
    'title'     => 'Популярные товары',
    'but_link'  => '/store?is_popular=1',
]); ?>
</div>
<div class="container container-pr mt-min">
    <div class="brend-image">
        <div class="brend-image__img">
            <?= CHtml::image($this->mainAssets . '/images/red.png','REDKIN') ?>
        </div>
    </div>
    <?php $this->widget('application.modules.store.widgets.ProductTypeWidget', [
    'condition' => 'is_sale = 1',
    'title'     => 'Товары со скидкой',
    'but_link'  => '/store?is_sale=1',
    'limit' => 100,
]); ?>
</div>
<section class="main">
    <div class="container d-flex ms-container">
        <main>
            <div class="main__head">
                <?= $page->title_short ?>
            </div>
            <div class="main__text">
                <?= $page->body ?>
            </div>
            <div class="main__buttons">
                <a href="/o-magazine" class="but but-black">
                    Больше информации
                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
               </a>
               <a href="/store" class="but bordered">
                   Перейти в каталог
               </a>
            </div>
        </main>
        <div class="widget-box">
            <?php $this->widget('application.modules.gallery.widgets.NewGalleryWidget',['id' => 1]); ?>
        </div>
    </div>
</section>