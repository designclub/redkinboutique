<?php if(!empty($categories)):?>
    <div class="filter-block-checkbox-list">
        <div class="filter-block-header">
            <strong><?= Yii::t('StoreModule.store', 'Categories');?></strong>
        </div>
        <div class="filter-block-body">
            <?php foreach($categories as $category):?>
                <?php foreach ($category->children as $key => $value): ?>
                <div class="checkbox">
                    <?= CHtml::checkBox('category[]',Yii::app()->attributesFilter->isMainSearchParamChecked(AttributeFilter::MAIN_SEARCH_PARAM_CATEGORY, $value->id, Yii::app()->getRequest()),['value' => $value->id, 'id' => 'category_'. $value->id]);?>
                    <?= CHtml::label($value->name, 'category_'. $value->id);?>
                </div>
                <?php endforeach ?>
            <?php endforeach;?>
        </div>
    </div>
    <hr/>
<?php endif;?>
