<?php if($category): ?>
	<div class="catalog-main">
	<?php foreach ($category as $key => $data) : ?>
			<a class="catalog-main__item" href="<?= $data->getCategoryUrl(); ?>">
					<div class="catalog-main__name">
						<?= $data->name_short; ?>
					</div>
					<div class="catalog-main__desc">
						<?= $data->short_description; ?>
					</div>
					<div class="catalog-main_link">
						<span>К покупкам</span>
						<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
					</div>
					<div class="catalog-main__img">
					<?php if($data->image) : ?>
						<?= CHtml::image($data->getImageUrl(), ''); ?>
					<?php else: ?>
						<?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/nophoto-product.jpg', ''); ?>
					<?php endif; ?>
				</div>
			</a>
	<?php endforeach; ?>
	</div>
<?php endif; ?>
