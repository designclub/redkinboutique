<?php if($models) : ?>
	<div class="product-section box-style">
				<div class="product-section__h2 box-style__h2">
					<span><?= $title; ?></span>
					<?= CHtml::link('Все товары', '/store') ?>
				</div>
			<div class="product-box product-box-carousel">
				<?php foreach ($models as $key => $data) : ?>
					<div>
						<?php Yii::app()->controller->renderPartial('//store/product/_item', ['data' => $data]) ?>
					</div>
				<?php endforeach; ?>
			</div>
	</div>
<?php endif; ?>