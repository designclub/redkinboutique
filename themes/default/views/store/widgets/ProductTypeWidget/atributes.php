<?php if($models) : ?>
    <div class="product-atributes filter-block price">
                <div class="product-section__h2 box-style__h2">
                    <div class="filter-block__header">
                    Размеры
                    </div>
                </div>
            <div class="product-atributes__box">
                <?php $count = 1; ?>
                <?php foreach ($models as $key => $data) : ?>
                    <?php foreach ($data->getAttributeGroups() as $groupName => $items) :
                                { ?>

                                            <?php
                                            foreach ($items as $attribute) :
                                                {
                                                $value = $data->attribute($attribute);
                                                    if($attribute->out){
                                                        continue;
                                                    }
                                                if (empty($value)) {
                                                    continue;
                                                }
                                                ?>
                                                <div class="atributes_wrap <?= ($count <= 6) ? '' : 'hidden'; ?>">
                                                      <label for="product-<?= $data->id; ?>">
                                                       </label >
                                                        <input type="checkbox" name="product-<?= $data->id; ?>"value="<?= AttributeRender::renderValue($attribute, $data->attribute($attribute)); ?>" id="product-<?= $data->id; ?>" class="ch">
                                                        <div class="value">
                                                            <?= AttributeRender::renderValue($attribute, $data->attribute($attribute)); ?>
                                                        </div>
                                                </div>

                                                <?php }
                                            endforeach; ?>
                                                    <?php }
                                                endforeach; ?>
                                                <?php $count++; ?>
                                    <?php endforeach; ?>
                                    <?php if($count > 6) : ?>
                    <a class="attr-block__more" href="#" style="text-decoration: none">
                        <span data-text="Скрыть">Показать еще (<?= $count - 7 ?>)</span>
                    </a>
                <?php endif; ?>
                                </div>
                        </div>
                    <?php endif; ?>
<?php
Yii::app()->clientScript->registerScript("checked", "
    $('.atributes_wrap label').on('click',function(){
        $(this).toggleClass('ckliked')
        })
");
 ?>