<div id="searchModal" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <?php $form = $this->beginWidget(
                    'bootstrap.widgets.TbActiveForm',
                    [
                        'action' => ['/store/product/index'],
                        'method' => 'GET',
                        'htmlOptions' => [
                            'class'  => 'form-inline'
                        ]
                    ]
                ) ?>
                    <div class="input-group search-input-group">
                         <?= CHtml::textField(
                            AttributeFilter::MAIN_SEARCH_QUERY_NAME,
                            CHtml::encode(Yii::app()->getRequest()->getQuery(AttributeFilter::MAIN_SEARCH_QUERY_NAME)),
                            ['placeholder' => 'Поиск товара', 'class' => 'form-control']
                        ); ?>
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default">
                                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/search.svg'); ?>
                            </button>
                        </span>
                    </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
