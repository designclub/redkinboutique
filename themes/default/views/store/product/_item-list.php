<div class="product-list__item">
    <div class="product-list__img">
		<a href="<?= ProductHelper::getUrl($data); ?>">
            <?= CHtml::image($data->getImageUrl(), CHtml::encode($data->getImageAlt()), ['title' => CHtml::encode($data->getImageTitle())]); ?>
    	</a>
    </div>
    <div class="product-list__content">
        <div class="product-list__info">
	        <div class="product-list__name">
	            <a class="product-list__link product-name" href="<?= ProductHelper::getUrl($data); ?>">
	                <?= $data->name; ?>
	            </a>
	        </div>
	        <div class="product-list__desc">
                <?= $data->short_description; ?>
	        </div>
        </div>
        <div class="product-list__section">
        	<div class="product-list__section_top">
	            <?php if(Yii::app()->hasModule('favorite')):?>
	                <div class="product-list__favorite product-favorite">
	                    <?php $this->widget('application.modules.favorite.widgets.FavoriteControl', [
	                        'product' => $data,
	                        'view' => "favorite-item"
	                    ]);?>
	                </div>
	            <?php endif;?>
        	</div>
        	<div class="product-list__section_bottom">
        		<div class="product-list__price product-price">
			        <?php if ($data->hasDiscount()) : ?>
			            <span class="product-price__old">
			                <span class="price-old"><?= round($data->getBasePrice(), 2) ?></span>
			                <span class="ruble">руб.</span>
			            </span><br>
			        <?php endif; ?>
			        <span class="price-result" id="result-price<?= $data->id?>"><?= round($data->getResultPrice(), 2); ?></span>
			        <span class="ruble">руб.</span>
			    </div>
			    <?php if (Yii::app()->hasModule('cart')) : ?>
			        <div class="product-list__button product-but-cart">
			            <a href="#" class="quick-add-product-to-cart but but-pink" data-product-id="<?= $data->id; ?>" data-cart-add-url="<?= Yii::app()->createUrl('/cart/cart/add');?>">
			                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/cart.svg'); ?>
			            </a>
			        </div>
			    <?php endif; ?>
        	</div>
		    <div class="clearfix"></div>
        </div>
    </div>
</div>