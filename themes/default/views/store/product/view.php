<?php

/* @var $product Product */

$this->title = $product->getMetaTitle();
$this->description = $product->getMetaDescription();
$this->keywords = $product->getMetaKeywords();
$this->canonical = $product->getMetaCanonical();

$mainAssets = Yii::app()->getModule('store')->getAssetsUrl();
Yii::app()->getClientScript()->registerCssFile( Yii::app()->getTheme()->getAssetsUrl() . '/css/shop.css' );
$this->breadcrumbs = array_merge(
    [Yii::t("StoreModule.store", 'Catalog') => ['/store/product/index']],
    $product->category ? $product->category->getBreadcrumbs(true) : [],
    [CHtml::encode($product->name)]
);
?>

<div class="page-content product-view-content" xmlns="http://www.w3.org/1999/html" itemscope itemtype="http://schema.org/Product">
    <div class="container">
        <div class="product-view">
            <div class="product-view__info">
                <div class="product-view__type product-type">
                    <?php foreach ($product->getBadges() as $key => $badge) : ?>
                        <div class="product-type__color">
                            <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/icon/'.$badge, ''); ?>
                            <span><?= $product->getAttributeLabel($key); ?></span>
                        </div>
                    <?php endforeach; ?>
                </div>

                <h1 itemprop="name"><?= CHtml::encode($product->getTitle()); ?></h1>

                <form action="<?= Yii::app()->createUrl('cart/cart/add'); ?>" method="post" data-max-value='<?= (int)$product->quantity ?>'>
                    <input type="hidden" name="Product[id]" value="<?= $product->id; ?>"/>
                    <?= CHtml::hiddenField(
                        Yii::app()->getRequest()->csrfTokenName,
                        Yii::app()->getRequest()->csrfToken
                    ); ?>
                 
                    <?php if($product->getVariantsGroup()):?>
                        <div class="products-attributes">
                        <?php 
                            $id = 0;
                         ?>
                        <?php foreach ($product->getVariantsGroup() as $title => $variantsGroup): ?>

                            <?php                                               
                                $id++;
                                $listData = CHtml::listData($variantsGroup, 'id', 'optionValue');
                            ?>
                        
                            <div class="products-attributes_attribute">
                                <label class = "label-attribute"><?= $title; ?>:</label>
                                <div class = "product-sizes id-<?= $product->id ?>">

                                    <?= ExtHtml::radioButtonList(
                                        "ProductVariant[]-{$id}-{$product->id}",
                                        current(array_keys($listData)),
                                        $listData,
                                        [
                                            'itemOptions' => $product->getVariantsOptions(), 
                                            'container' => '',
                                            'class' => 'js-size-variant-product',
                                            'separator' => '',                                  
                                            'template' => "
                                                <div class=\"products-sizes products-sizes_size id-col-{$product->id}\">
                                                    {input}
                                                    {label}
                                                </div>"
                                        ]
                                    ); ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <?php endif;?>
                        <?php //Цена ?>
                    <div class="product-view__price product-price <?= ($product->hasDiscount()) ? 'product-price__new' : '' ?>" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                        <input type="hidden" id="base-price" value="<?= round($product->getBasePrice(), 2) ?>"/>
                        <div class="product-view__price_heading">Цена:</div>
                        <span class="product-price__result">
                            <span class="price-result" itemprop="price" id="result-price<?= $product->id?>"><?= round($product->getBasePrice(), 2) ?></span>
                            <span class="ruble">Руб.</span>
                        </span>
                        <?php if ($product->hasDiscount()) : ?>
                            <span class="product-price__old">
                                <span class="price-old"><?= round($product->getResultPrice(), 2); ?></span>
                            </span><br>
                        <?php endif; ?>
                        <meta itemprop="priceCurrency" content="<?= Yii::app()->getModule('store')->currency?>">
                            <?= $product->isInStock() ? '<link itemprop="availability" href="http://schema.org/InStock">' : '<link itemprop="availability" href="http://schema.org/PreOrder">';?>
                    </div>
                    <?php if (Yii::app()->hasModule('order')) : ?>

                        <div class="product-view__button product-button">
                            <?php if (Yii::app()->hasModule('cart')) : ?>
                                <div class="product-button__item product-cart">
                                    <button class="btn but but-black but-add-cart" id="add-product-to-cart">
                                        <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/cart.svg'); ?>
                                        <span>Добавить в корзину</span>
                                    </button>
                                </div>
                            <?php endif; ?>
                            <?php if(Yii::app()->hasModule('favorite')):?>
                                <div class="product-button__item product-favorite">
                                    <?php $this->widget('application.modules.favorite.widgets.FavoriteControl', [
                                        'product' => $product,
                                        'view' => "favorite"
                                        // 'view' => "favorite-item"
                                    ]);?>
                                </div>
                            <?php endif;?>
                        </div>
                        <div class="product-view__hidden hidden">
                            <span id="product-result-price"><?= round($product->getBasePrice(), 2) ?></span> x
                            <span id="product-quantity">1</span> =
                            <span id="product-total-price"><?= round($product->getBasePrice(), 2) ?></span>
                            <span class="ruble"> <?= Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency); ?></span>
                        </div>
                    <?php endif; ?>
                </form>
               <div class="desc_title">Описание:</div>
                <div class="properties properties-product-view">
                            <?php foreach ($product->getAttributeGroups() as $groupName => $items) :
                                { ?>

                                            <?php
                                            foreach ($items as $attribute) :
                                                {
                                                $value = $product->attribute($attribute);

                                                if (empty($value)) {
                                                    continue;
                                                }
                                                ?>
                                                    <div class="key-value">
                                                    <div class="key">
                                                        <span><?= CHtml::encode($attribute->title); ?>:</span>
                                                    </div>
                                                    <div class="value">
                                                        <?= AttributeRender::renderValue($attribute, $product->attribute($attribute)); ?>
                                                    </div>
                                                    </div>
                                                <?php }
                                            endforeach; ?>


                                <?php }
                            endforeach; ?>
                        </div>
                        <div class="product-view__desc" itemprop="description">
                    <?= $product->short_description; ?>
                </div>
            </div>
            <?php $images = $product->getImages(); ?>
            <div class="product-view__img <?= (count($images) > 0) ? '' : 'product-view__img_2'; ?>">
                 <!-- Миниатюры -->
                <?php if (count($images) > 0) : ?>
                    <div class="image-thumbnail">
                        <div>
                            <div class="image-thumbnail__box">
                                <div class="image-thumbnail__img">
                                    <img src="<?= StoreImage::product($product); ?>" />
                                </div>
                            </div>
                        </div>
                        <?php foreach ($images as $key => $image) : ?>
                            <div>
                                <div class="image-thumbnail__box">
                                    <div class="image-thumbnail__img">
                                        <?= CHtml::image($image->getImageUrl(), '', ['style'=>''])?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                    <?php Yii::app()->getClientScript()->registerScript(
                        "product-image-thumbnail",
                        "
                            $('.image-thumbnail').slick({
                                slidesToShow: 3,
                                slidesToScroll: 1,
                                asNavFor: '.image-preview',
                                dots: false,
                                arrows: false,
                                focusOnSelect: true,
                                vertical: true,
                                responsive: [
                                    {
                                        breakpoint: 641,
                                        settings: {
                                            vertical: false,
                                            slidesToShow: 3,
                                            slidesToScroll: 1,
                                        }
                                    },
                                ]
                            });
                    "
                    ); ?>
                <?php endif; ?>
                <div class="image-preview">
                    <div>
                        <div class="image-preview__img">
                            <a data-fancybox="image" href="<?= StoreImage::product($product); ?>">
                                <img
                                    class="gallery-image"
                                    src="<?= StoreImage::product($product); ?>"
                                    itemprop="image"
                                />
                            </a>
                        </div>
                    </div>
                    <?php foreach ($images as $key => $image) : ?>
                        <div>
                            <div class="image-preview__img">
                                <a data-fancybox="image" href="<?= $image->getImageUrl(); ?>">
                                    <?= CHtml::image($image->getImageUrl(), '', [
                                        'class' => 'gallery-image'
                                    ])?>
                                </a>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
                <?php Yii::app()->getClientScript()->registerScript(
                    "product-image-preview",
                    "
                        $('.image-preview').slick({
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            fade: true,
                            dots: false,
                            arrows: false,
                            asNavFor: '.image-thumbnail',
                            responsive: [
                                {
                                    breakpoint: 480,
                                    settings: {
                                        arrows: false,
                                    }
                                }
                            ]
                        });
                "
                ); ?>


            </div>
        </div>
       <!-- decription -->
    </div>
</div>
<div class="reviews-wrap">
    <div class="container">
     <?php $this->widget('application.modules.review.widgets.ReviewNewWidget',['view' => 'review', 'limit' => 2]); ?>
    </div>
</div>

    <?php $this->widget('application.modules.store.widgets.LinkedProductsWidget',['product' => $product, 'code' => null,]); ?>

<?php $this->widget('application.modules.review.widgets.ReviewWidget',['view' => 'reviewmodalwidget']); ?>
<?php
$fancybox = $this->widget(
    'gallery.extensions.fancybox3.AlFancybox',
    [
        'target' => '[data-fancybox]',
        'lang'   => 'ru',
        'config' => [
            'animationEffect' => "fade",
            'buttons' => [
                "zoom",
                "close",
            ]
        ],
    ]
);
?>
<?php Yii::app()->getClientScript()->registerScript(
    "product-myTab",
    <<<JS
        $("#myTab li").first().addClass('active');
        $(".tab-pane").first().addClass('active');
JS
); ?>
