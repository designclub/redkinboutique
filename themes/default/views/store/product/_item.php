<div class="product-box__item">
    <div class="product-item__head">
    <?php if ($data->is_popular): ?>
        <div class="best">Лучшее</div>
    <?php endif ?>
    <?php if ($data->is_sale): ?>
        <div class="best sale">Распродажа</div>
    <?php endif ?>
      <?php if(Yii::app()->hasModule('favorite')):?>
            <div class="product-button__item product-favorite">
                <?php $this->widget('application.modules.favorite.widgets.FavoriteControl', [
                    'product' => $data,
                    'view' => "favorite-item"
                ]);?>
            </div>
        <?php endif;?>
    </div>
    <a href="<?= ProductHelper::getUrl($data); ?>">
        <div class="product-box__img" style="position: relative">
            <?= CHtml::image($data->getImageUrl(), CHtml::encode($data->getImageAlt()), ['title' => CHtml::encode($data->getImageTitle())]); ?>
             <?php if ($data->is_sale) : ?>
                <div class="discount_box" style="position: absolute">
                    -<?= round($data->getDiscount())?>%
                </div>
             <?php endif; ?>
        </div>
    </a>

    <div class="product-box__info">

            <div class="product-box__name">
                <span><?= $data->name; ?></span>
            </div>
        <div class="product-box__bottom">

                    <form action="<?= Yii::app()->createUrl('cart/cart/add'); ?>" method="post" data-max-value='<?= (int)$data->quantity ?>'>
                    <input type="hidden" name="Product[id]" value="<?= $data->id; ?>"/>
                    <?= CHtml::hiddenField(
                        Yii::app()->getRequest()->csrfTokenName,
                        Yii::app()->getRequest()->csrfToken
                    ); ?>
                        <?php //Цена ?>
                    <div class="product-view__price product-price <?= ($data->hasDiscount()) ? 'product-price__new' : '' ?>" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                        <input type="hidden" id="base-price" value="<?= round($data->getResultPrice(), 2); ?>"/>
                        <div class="prices-spans">

                        <span class="product-price__result <?= $data->hasDiscount()? 'sale-price': ''?>">
                            <?php if ($data->hasDiscount()) : ?>
                            <span class="price-result" itemprop="price" id="result-price<?= $data->id?>"><?= round($data->getBasePrice(), 2) ?></span>
                            <?php else: ?>
                                <span class="price-result" itemprop="price" id="result-price<?= $data->id?>"><?= round($data->getResultPrice(), 2); ?></span>
                             <?php endif; ?>
                            <span class="ruble">руб.</span>
                        </span>
                        <?php if ($data->hasDiscount()) : ?>
                            <span class="product-price__old">
                                <span class="price-old"><?= round($data->getResultPrice(), 2); ?></span>
                            </span>
                        <?php endif; ?>
                        </div>
                        <meta itemprop="priceCurrency" content="<?= Yii::app()->getModule('store')->currency?>">
                            <?= $data->isInStock() ? '<link itemprop="availability" href="http://schema.org/InStock">' : '<link itemprop="availability" href="http://schema.org/PreOrder">';?>
                    </div>

                    <div class="by-order ">
                    <div class="properties">
                            <?php foreach ($data->getAttributeGroups() as $groupName => $items) :
                                { ?>

                                            <?php
                                            foreach ($items as $attribute) :
                                                {
                                                $value = $data->attribute($attribute);
                                                    if($attribute->out){
                                                        continue;
                                                    }
                                                if (empty($value)) {
                                                    continue;
                                                }
                                                ?>

                                                    <div class="key">
                                                        <span><?= CHtml::encode($attribute->title); ?>:</span>
                                                    </div>
                                                    <div class="value">
                                                        <?= AttributeRender::renderValue($attribute, $data->attribute($attribute)); ?>
                                                    </div>

                                                <?php }
                                            endforeach; ?>


                                <?php }
                            endforeach; ?>
                        </div>
                    <div class="by-order__flex">
                    <a class="product-box__link product-name" href="<?= ProductHelper::getUrl($data); ?>">
                        <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/view.svg'); ?>
                    </a>
                    <?php if (Yii::app()->hasModule('order')) : ?>
                        <div class="product-view__spinput">
                            <?php
                                $minQuantity = 1;
                                $maxQuantity = Yii::app()->getModule('store')->controlStockBalances ? $data->getAvailableQuantity() : 99;
                            ?>
                            <span data-min-value='<?= $minQuantity; ?>' data-max-value='<?= $maxQuantity; ?>'
                                  class="spinput js-spinput">
                                <span class="spinput__minus js-spinput__minus product-quantity-decrease"></span>
                                <input name="Product[quantity]" value="1" class="spinput__value product-quantity-input"
                                       id="product-quantity-input"/>
                                <span class="spinput__plus js-spinput__plus product-quantity-increase"></span>
                            </span>
                        </div>

                            <?php if (Yii::app()->hasModule('cart')) : ?>
                                <div class="product-button__item product-cart">
                                    <button class="btn but <?= $data->is_sale?'but-pink':'but-black'?> but-add-cart" id="add-product-to-cart">
                                        <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/cart.svg'); ?>
                                        <span>купить</span>
                                    </button>
                                </div>
                            <?php endif; ?>
                            </div>
                        </div>
                        <div class="product-view__hidden hidden">
                            <span id="product-result-price"><?= round($data->getResultPrice(), 2); ?></span> x
                            <span id="product-quantity">1</span> =
                            <span id="product-total-price"><?= round($data->getResultPrice(), 2); ?></span>
                            <span class="ruble"> <?= Yii::t("StoreModule.store", Yii::app()->getModule('store')->currency); ?></span>
                        </div>
                    <?php endif; ?>
                </form>

        </div>
    </div>
</div>
