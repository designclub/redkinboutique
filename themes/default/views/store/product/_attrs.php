<div class="product-view__spinput">
                            <?php
                                $minQuantity = 1;
                                $maxQuantity = Yii::app()->getModule('store')->controlStockBalances ? $product->getAvailableQuantity() : 99;
                            ?>
                            <span data-min-value='<?= $minQuantity; ?>' data-max-value='<?= $maxQuantity; ?>'
                                  class="spinput js-spinput">
                                <span class="spinput__minus js-spinput__minus product-quantity-decrease">-</span>
                                <input name="Product[quantity]" value="1" class="spinput__value product-quantity-input"
                                       id="product-quantity-input"/>
                                <span class="spinput__plus js-spinput__plus product-quantity-increase">+</span>
                            </span>
                        </div>


                         <div class="clearfix"></div>
        <div class="product-view-info">
            <ul class="product-view-info__tabs" id="myTab">
                <?php if (!empty($product->description)): ?>
                    <li>
                        <a href="#description" data-toggle="tab"><?= Yii::t("StoreModule.store", "Description"); ?></a>
                    </li>
                <?php endif; ?>
                <?php if (!empty($product->data)): ?>
                    <li><a href="#data" data-toggle="tab"><?= Yii::t("StoreModule.store", "Характеристики"); ?></a></li>
                <?php endif; ?>
            </ul>

            <div class="product-view-info__tab-content tab-content">
                <?php if (!empty($product->description)): ?>
                    <div class="tab-pane" id="description" itemprop="description">
                        <?= $product->description; ?>
                    </div>
                <?php endif; ?>
                <?php if (!empty($product->data)): ?>
                    <div class="tab-pane" id="data">
                        <?= $product->data; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>


        <h1><?= CHtml::encode($category->getTitle()); ?></h1>
        <?php $this->widget('application.modules.store.widgets.CategoryWidget',['parent' => $category->id,'view'=>'category-filter']); ?>