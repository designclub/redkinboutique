<?php
$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
/* @var $category StoreCategory */

$this->title =  $category->getMetaTitle();
$this->description = $category->getMetaDescription();
$this->keywords =  $category->getMetaKeywords();
$this->canonical = $category->getMetaCanonical();

$this->breadcrumbs = [Yii::t("StoreModule.store", "Catalog") => ['/store/product/index']];

$this->breadcrumbs = array_merge(
    $this->breadcrumbs,
    $category->getBreadcrumbs(true)
);

?>

<div class="page-content category-content">
    <div class="container">
        <h1><?= CHtml::encode($category->getTitle()); ?></h1>
        <div class="category-box">
            <div class="category-box__sidebar">
                <div class="sidebar-box">
                    <div class="sidebar-box__close"><div></div></div>
                    <form id="store-filter" name="store-filter" method="get">
                        <div class="filter-content">
                            <?php if($this->beginCache('store::category::count:'.$category->id)):?>
                                <?php $this->widget('application.modules.store.widgets.CategoryWidget', [
                                    'view' => 'category-with-count',
                                    'parent' => $category->id
                                ]); ?>
                                <?php $this->endCache(); ?>
                            <?php endif;?>
                            <?php $this->widget('application.modules.store.widgets.filters.PriceFilterWidget'); ?>

                            <?php $this->widget('application.modules.store.widgets.filters.FilterBlockWidget', [
                                'category' => $category
                            ]); ?>
                        </div>
                           <div class="filter-block filter-block-header">
                                <div class="filter-block__body">
                                    <button type="reset" class="but but-reset but-black">
                                       <span>Очистить фильтры</span>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
            <div class="category-box__content">
                <div id="selected-filters" class="selected-filters"></div>
                <?php
                    $this->widget(
                        // 'application.components.MyClistView',
                        'bootstrap.widgets.TbListView',
                    [
                        'dataProvider' => $dataProvider,
                        'id' => 'product-box',
                        // 'itemView' => '//store/product/_item',
                        'itemView' => '//store/product/'.$this->storeItem,
                        'emptyText'=>'В данной категории нет товаров.',
                        'summaryText'=>"{count} тов.",
                        'template'=>'
                            <a class="but-menu-filter" href="#"><i class="fa fa-filter" aria-hidden="true"></i><span>Фильтры</span></a>
                            <div class="catalog-controls">
                                <div class="catalog-controls__sort">
                                    <div class="sort-box">
                                        <div class="sort-box__header">Сортировать по: </div>
                                        <div class="sort-box__body sort-box-wrapper">
                                            <div class="sort-box-wrapper__header">По возрастанию цены</div>
                                            <div class="sort-box-wrapper__body">
                                                <div class="sort-box-wrapper__link" data-href="?sort=price">Сначала недорогие</div>
                                                <div class="sort-box-wrapper__link" data-href="?sort=price.desc">Сначала дорогие</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="catalog-controls__res">
                                    {summary}
                                    <div class="template-product">
                                        <div data-view="_item" class="template-product__item template-product__grid '.($this->storeItem == "_item" ? "active" : "" ).'">'. file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/product-grid.svg') .'</div>
                                        <div data-view="_item" class="template-product__item template-product__list '.($this->storeItem == "_item-list" ? "active" : "" ).'">'. file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/product-list.svg') .'</div>
                                    </div>
                                </div>
                            </div>
                            {items}
                            {pager}
                        ',
                        'sortableAttributes' => [
                            'name',
                            'price',
                            'update_time'
                        ],
                        'itemsCssClass' => 'product-box product-list',
                        'htmlOptions' => [
                            // 'class' => 'product-box'
                        ],
                        'ajaxUpdate'=>true,
                        'enableHistory' => true,
                        // 'ajaxUrl'=>'GET',
                        'pagerCssClass' => 'pagination-box',
                        'pager' => [
                            'header' => '',
                            'lastPageLabel' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
                            'firstPageLabel' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
                            // 'lastPageLabel'  => false,
                            // 'firstPageLabel' => false,
                            'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                            'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                            'maxButtonCount' => 5,
                            'htmlOptions' => [
                                'class' => 'pagination'
                            ],
                        ]
                    ]
                ); ?>
            </div>
        </div>
    </div>
</div>

       <?php $this->widget('application.modules.viewed.widgets.ViewedWidget',['limit' => 5]); ?>

<?php /*
<div class="company-home">
    <div class="content">
        <div class="company-home__left">
            <?php $this->widget('application.modules.page.widgets.PagesNewWidget', [
                'id' => 6,
                'view' => 'company-home'
            ]); ?>
        </div>
        <div class="company-home__right">
            <?php $this->widget('application.modules.stock.widgets.StockWidget', [
                'limit' => 4,
                'view' => 'stock-home'
            ]); ?>
        </div>
    </div>
</div>
 /*?>