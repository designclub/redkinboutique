<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->title_short = $model->title_short;
$this->under_title = $model->under_title;
$this->slug = $model->slug;
?>

<div class="catalog_wrap bgr">
       <div class="container">
            <div class="row">
                <div class="col-md-12">
                  <div class="calculation">
                      <?php $this->widget('application.modules.page.widgets.CalculatorWidget') ?>
                  </div>
                </div>
            </div>
        </div>
</div>
<?php
Yii::app()->clientScript->registerScript("print", "
    $(document).delegate('#print','click', function(){
     var printing_css='<style media=print>tr:nth-child(even) td{background: #f0f0f0;}</style>';
        var html_to_print=printing_css+$('#calculator-form').html();
        var iframe=$('<iframe id=\"print_frame\">');
        $('body').append(iframe);
        var doc = $('#print_frame')[0].contentDocument || $('#print_frame')[0].contentWindow.document;
        var win = $('#print_frame')[0].contentWindow || $('#print_frame')[0];
        doc.getElementsByTagName('body')[0].innerHTML=html_to_print;
        win.print();
        $('iframe').remove();
})
  ")
 ?>


