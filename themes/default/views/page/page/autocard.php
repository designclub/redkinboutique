<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->title_short = $model->title_short;
$this->under_title = $model->under_title;
?>
<div class="container">
    <h2 class="page_title line"><?= $model->title_short ?></h2>
</div>
<div class="container d-flex auto-card">
    <div class="photos">
        <div class="img-main">
            <?php if (!empty($model->image)): ?>
            <?= CHtml::image($model->getImageUrl(0,0,true,null,"image")) ?>
             <?php else: ?>
             <?= CHtml::image($this->mainAssets . '/images/auto.jpg') ?>
            <?php endif ?>
        </div>
        <div class="row-flex">
            <div class="imgs">
                <?php if (!empty($model->image)): ?>
            <?= CHtml::image($model->getImageUrl(0,0,true,null,"image")) ?>
             <?php else: ?>
             <?= CHtml::image($this->mainAssets . '/images/auto.jpg') ?>
            <?php endif ?>
            </div>
            <div class="imgs">
              <?php if (!empty($model->svg)): ?>
            <?= CHtml::image($model->getImageUrl(0,0,true,null,"svg")) ?>
             <?php else: ?>
             <?= CHtml::image($this->mainAssets . '/images/auto.jpg') ?>
            <?php endif ?>
            </div>
            <div class="imgs">
                <?php if (!empty($model->img)): ?>
            <?= CHtml::image($model->getImageUrl(0,0,true,null,"img")) ?>
             <?php else: ?>
             <?= CHtml::image($this->mainAssets . '/images/auto.jpg') ?>
            <?php endif ?>
            </div>
        </div>
    </div>
    <div class="table-flex">
      <div class="table_box">
          <div class="table-row">
              <div class="td">Грузоподъемность</div>
              <div class="td2">
                <?= $model->year ? $model->year : '1 230 – 2 480 кг' ?>
             </div>
          </div>
          <div class="table-row">
              <div class="td">Привод</div>
              <div class="td2">
                <?= $model->previewtext ? $model->previewtext : 'задний 4×2' ?>
             </div>
          </div>
          <div class="table-row">
              <div class="td">Количество мест</div>
              <div class="td2">
                <?= $model->promo ? $model->promo : 'MRT 17+1' ?>
              </div>
          </div>
          <div class="table-row">
              <div class="td">Длина</div>
              <div class="td2">
               <?= $model->leng ? $model->leng : '6590/6995 мм' ?>
             </div>
          </div>
          <div class="table-row">
              <div class="td">Емкость бака</div>
              <div class="td2">
                <?= $model->tank ? $model->tank : '75 л' ?>
             </div>
          </div>
          <div class="table-row heating">
              <div class="td">Отопление</div>
              <div class="td2">
              <p>
                <?= $model->heating ? $model->heating : 'Тосольного типа, 8 кВт с распределением потока на 3 дефлектора' ?>
             </p>
             </div>
          </div>
      </div>
      <div class="arenda_price">
          <div class="name">Стоимость аренды <br>за 1 маш/час, руб с НДС</div>
          <div class="price">
            <?= $model->rents ? $model->rents : '2150 руб.' ?>
        </div>
      </div>
      <div class="link_to_tarif">
          <a href="tarify-perevozok">Посмотреть все тарифы перевозок</a>
      </div>
    </div>
</div>
<div class="category-widget_wrap">
    <div class="container">
        <h2 class="page_title line">Ещё из этой категории</h2>
        <?php $this->widget('application.modules.page.widgets.PagesWidget',[
                'parent_id' => $model->parent_id,
                'view' => 'category_auto',
                'delete' => "id <> {$model->id}",
            ]); ?>
    </div>
</div>
<?php
Yii::app()->clientScript->registerScript("autocard", "

 $('.imgs').on('click', function(){
  var srcs = $(this).find('img').attr('src');
  $('.auto-card .photos .img-main img').attr('src', srcs);
 })

");
 ?>
