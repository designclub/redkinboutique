<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->title_short = $model->title_short;
$this->under_title = $model->under_title;
?>
<div class="container">
  <h1 class="page_title">
    <?= $model->title_short ?>
  </h1>
</div>
<section class="contacts">
  <div class="container d-flex">
    <div class="map-wrap">
      <div class="map_box">
        <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A582448908c961c93a416e5e05f1b68f98f94c0953027a0aacb4c18e6305252e5&amp;source=constructor" width="100%" height="478" frameborder="0"></iframe>
      </div>
    </div>
      <div class="contacts-wrap">
        <div class="contacts_box">
          <div class="contacts_box__title">Контакты</div>
          <div class="contacts_box__item">
            <small>Мы находимся по адресу:</small>
            <div class="detail">
              Оренбург, Расковой 10А, <br>
              10 вход, 3 этаж, 3 кабинет
            </div>
          </div>
          <div class="contacts_box__item">
            <small>E.mail:</small>
            <a href="mailto:<?=Yii::app()->getModule('yupe')->email?>" class="detail">
              <?=Yii::app()->getModule('yupe')->email?>
            </a>
          </div>
          <div class="contacts_box__item">
            <small>Телефон для связи:</small>
            <a href="tel:<?= Yii::app()->getModule('yupe')->p_reception ?>" class="detail">
              <?= Yii::app()->getModule('yupe')->reception ?>
            </a>
          </div>
        </div>
      </div>
  </div>
</section>
<section class="form-section">
  <div class="container form-container">
  <?php $this->widget('application.modules.mail.widgets.ContactFormsWidget'); ?>
  </div>
  <div class="section_bg">
    <?= CHtml::image($this->mainAssets . '/images/soyuz.png', 'Правовая компания "СОЮЗ"') ?>
  </div>
</section>
