<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->title_short = $model->title_short;
$this->under_title = $model->under_title;
?>
    <div class="services_wrap nobifore">
         <div class="container">
           <h2 class="page_title"><?= $model->title_short ?></h2>
           <div class="services">
               <?php $this->widget('application.modules.page.widgets.PagesWidget',['parent_id' => 99, 'view' => 'services']); ?>
           </div>
         </div>
     </div>
     <section class="form-section">
        <div class="container form-container">
        <?php $this->widget('application.modules.mail.widgets.ContactFormsWidget'); ?>
        </div>
        <div class="section_bg">
          <?= CHtml::image($this->mainAssets . '/images/soyuz.png', 'Правовая компания "СОЮЗ"') ?>
        </div>
    </section>
