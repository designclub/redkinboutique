<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->title_short = $model->title_short;
$this->under_title = $model->under_title;
?>
<div class="servicePage">
  <div class="container servicePage__container">
    <div class="servicePage__container--main">
      <h1 class="page_title"><?= $model->title_short ?></h1>
      <small>
        <?php if (!empty($model->under_title)): ?>
          <?= $model->under_title ?>
          <?php else: ?>
            Выражение «Покупатель всегда прав!» для нас не пустой звук - мы умеем доказывать его на деле
        <?php endif ?>
      </small>
      <?php if (!empty($model->body)): ?>
      <?= $model->body ?>
        <?php else: ?>
              <p>
                Любой гражданин, который приобретает или заказывает товары, работы или услуги для личных, семейных, домашних нужд, является потребителем и на него распространяется действие  Федерального Закона «О защите прав потребителей». Даже такие крупные приобретения как квартира, автомобиль, турпутевка, строительство дома подпадает под действие данного закона. Но приобритение для осуществления предпринимательской деятельности, в коммерческих целях, за счет или в интересах своей фирмы (работодателя) не подпадает под действие закона.
              </p>
      <?php endif ?>
    </div>
    <div class="servicePage__container--menu">
      <div class="container--menu__title">
         Помощь<br>
         семейного юриста
      </div>
       <?php $this->widget('application.modules.page.widgets.PagesWidget',[
                'parent_id' => $model->parent_id,
                'view' => 'service-list',
                'delete' => "id <> {$model->id}",
            ]); ?>
    </div>
    <div class="servicePage__container--modeWork">
      <div class="modeWork__title">
        Порядок работы
      </div>
      <div class="modeWork__list">
        <?php $this->widget('application.modules.contentblock.widgets.ContentBlockWidget',['code' => 'poryadok-raboty']); ?>
      </div><!-- /modeWork__list -->
    </div><!-- /servicePage__container--modeWork -->
    <div class="servicePage__container--form">
        <div class="container--form__title">
          Короткий слоган, с приведенной <br>
          ссылкой для перехода <br>
          в другой раздел
        </div>
        <div class="container--form__victoriyes">
          <div class="victoriyes__img">
            <?= CHtml::image($this->mainAssets . '/images/gavel.png') ?>
          </div>
          <div class="victoriyes__text">
            Наши одержанные победы
          </div>
        </div>
        <div class="container--form__accept">
          Перед окончательным решением,<br> убедитесь в нас, поговорив <br>
          с нашими специалистами
        </div>
        <a href="#" class="long" data-target="#callbackModal" data-toggle="modal"><span>Получить обратную связь</span></a>
    </div>
  </div>
  <div class="container srvice-prices">
    <div class="srvice-prices__title">Стоимость услуги</div>
    <div class="srvice-prices__item">
      <div class="srvice-prices__item--name">
        Консультация по телефону, онлайн консультация
      </div>
      <div class="srvice-prices__item--price">
        Бесплатно
      </div>
    </div>
    <?php if (!empty($model->short_content)): ?>
        <?= $model->short_content ?>
        <?php else: ?>
          <div class="srvice-prices__item">
            <div class="srvice-prices__item--name">
              Личная встреча и подробное обсуждение дела
            </div>
            <div class="srvice-prices__item--price">
              2000 - 4000 р
            </div>
        </div>
        <div class="srvice-prices__item">
            <div class="srvice-prices__item--name">
              Составление претензии (жалобы)
            </div>
            <div class="srvice-prices__item--price">
              от 3000 р
            </div>
        </div>
        <div class="srvice-prices__item">
            <div class="srvice-prices__item--name">
              Взыскание денег через Суд
            </div>
            <div class="srvice-prices__item--price">
              от 25000 р
            </div>
        </div>
      <?php endif ?>
  </div>
</div>
<section class="form-section">
  <div class="container form-container">
  <?php $this->widget('application.modules.mail.widgets.ContactFormsWidget'); ?>
  </div>
  <div class="section_bg">
    <?= CHtml::image($this->mainAssets . '/images/soyuz.png', 'Правовая компания "СОЮЗ"') ?>
  </div>
</section>