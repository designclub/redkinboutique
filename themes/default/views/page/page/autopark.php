<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->title_short = $model->title_short;
$this->under_title = $model->under_title;
?>
<div class="container tabs-autopark header_of_page">
    <h2 class="page_title line"><?= $model->title ?></h2>
    <a href="tarify-perevozok" class="tarif">Тарифная сетка</a>
</div>
<div class="container tabs-autopark">
    <?php $this->widget('application.modules.page.widgets.PagesWidget',['parent_id' => 61, 'view' => 'tabs-autopark']); ?>
</div>

