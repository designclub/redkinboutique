<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
$this->title_short = $model->title_short;
$this->under_title = $model->under_title;
?>
<div class="container">
    <h2 class="page_title page service-page-head"><?= $model->title_short ?></h2>
</div>
<div class="container d-flex service-page_container">
    <div class="service_img">
      <div class="img">
        <?php if (!empty($model->image)): ?>
          <?= CHtml::image($page->getImageUrl(569,482,true,null,"image")) ?>
          <?php else: ?>
            <?= CHtml::image($this->mainAssets . '/images/serv.jpg') ?>
        <?php endif ?>
      </div>
    </div>
    <div class="services_content">
      <div class="services_content__icons">
        <div class="icons_box">
          <div class="icon">
            <?php include 'support.svg' ;?>
          </div>
          <div class="text">
            приедем в течении 50 минут <br>
              после назанчения заказа
          </div>
        </div>
        <div class="icons_box">
          <div class="icon">
            <?php include 'clock.svg' ;?>
          </div>
          <div class="text">
            обговорим с вами все детали<br>
            будущего заказа
          </div>
        </div>
      </div>
      <div class="body">
        <?php if (!empty($model->body)): ?>
          <?= $model->body ?>
          <?php else: ?>
            <ul>
              <li>Проверяем возможность видеоинспекции</li>
              <li>Настраиваем спец. оборудование, определяем длину</li>
              <li>Производим процедуру, снимаем показания прибора</li>
              <li>Передаем данные, сопровождая их комментариями</li>
            </ul>
            <p>Видеодиагностика канализации потребуется, когда неисправность системы на лицо, а выявить причину невозможно без детального обследования. Сэкономить средства на замену трубопровода возможно благодаря современному обследованию видеокамерой.</p>
            <p>Обследование трубопровода данным методом позволяет определить изгибы магистрали, их протяженность и конфигурацию, причины засора и расположение колодца (при наличии).</p>
        <?php endif ?>
      </div>
      <a href="#" class="orange" data-target="#callbackModal" data-toggle="modal">Заказать услугу</a>
    </div>
</div>
<div class="other-services">
  <div class="container">
    <h2 class="page_title page">Другие наши услуги</h2>
<?php $this->widget('application.modules.page.widgets.PagesWidget',[
                'parent_id' => $model->parent_id,
                'view' => 'page_list',
                'delete' => "id <> {$model->id}",
            ]); ?>
  </div>
</div>
<div class="service-form">
  <div class="container d-flex">
    <div class="form_description">
      <h2 class="page_title">Свяжитесь с нами:</h2>
      <div class="form_text">
        Мы поможем выбрать тип услуги, количество необходимой <br>спецтехники и специалистов, рассчитаем стоимость<br>
          и количество времени
      </div>
      <a href="#" class="orange" data-target="#callbackModal" data-toggle="modal">Заполнить форму</a>
    </div>
      <div class="form_image"></div>
  </div>
</div>



