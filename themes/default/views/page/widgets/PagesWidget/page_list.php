   <ul class="page_list">
   <?php foreach ($pages as $key => $page) : ?>
      <li>
        <a href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]) ?>">
          <?= $page->title_short ?>
        </a>
        <div class="desc">
            текст с описанием услуги в
            две или три строки
        </div>
      </li>
    <?php endforeach;?>
   </ul>
