<div class="technika">
  <?php foreach ($pages as $key => $page) : ?>
      <div class="technika_items">
          <div class="technika_items__img" style="position: relative">
            <?= CHtml::image($page->getImageUrl(371,314,true,null,"icon")) ?>
            <?php if (!empty($page->under_title)): ?>
            <div class="stock" style="position: absolute">
              <span><?= $page->under_title ?></span>
            </div>
            <?php endif ?>
          </div>
          <div class="technika_items__name">
            <a href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]) ?>"><?= $page->title?></a>
          </div>
          <div class="short_content">
             <a href="#" data-target="#callbackModal" data-toggle="modal" class="orange">Заказать</a>
         </div>
      </div>
    <?php endforeach;?>
</div>
<?php
Yii::app()->clientScript->registerScript("category", "
   $('.technika').slick({
    autoplay: false,
    arrows: true,
    ifinity:true,
    dots:false,
    slidesToShow: 3,
    autoplaySpeed:5000,
    // appendDots:'.dots_container',
    responsive: [
                {
                    breakpoint: 813,
                    settings: {
                        slidesToShow: 2,
                        arrows: true,
                        dots:false
                    }
                },
                 {
                    breakpoint: 555,
                    settings: {
                        slidesToShow: 1,
                        arrows: false,
                        dots:false
                    }
                },
            ]
    });
");
?>
