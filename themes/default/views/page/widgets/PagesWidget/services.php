  <?php foreach ($pages as $key => $page) : ?>
        <a class="services_item" style="position: relative" href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]) ?>">
           <div class="services_item__img">
              <?= CHtml::image($page->getImageUrl(0,0,true,null,"icon"),$page->title_short) ?>
           </div>
           <div class="services_item__name">
             <?= $page->title?>
           </div>
           <div class="services_item__text">
             текст с описанием услуги в одну или две строки
           </div>
        </a>
    <?php endforeach;?>
