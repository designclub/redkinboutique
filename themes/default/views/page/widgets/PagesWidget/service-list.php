
      <div class="main-service-list">
          <?php foreach ($pages as $key => $page): ?>
              <div class="main-service__item">
                  <div class="item__img">
                    <?php if (!empty($page->image)): ?>
                      <?= CHtml::image($page->getImageUrl(0,0,true,null,"image")) ?>
                        <?php else: ?>
                        <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/serv.jpg') ?>
                    <?php endif ?>
                  </div>
                  <div class="item__name">
                      <?= CHtml::link($page->title_short, ['/page/page/view', 'slug' => $page->slug]) ?>
                  </div>
              </div>
          <?php endforeach ?>
      </div>
