 <ul class="nav nav-tabs" role="tablist">
<?php foreach ($pages as $key => $page) : ?>
    <li role="presentation" class="<?= $key===0 ? 'active' : '' ?>">
         <a href="#<?= $page->id?>" aria-controls="home" role="tab" data-toggle="tab">
         <?= $page->title ?>
         </a>
   </li>
<?php endforeach;?>
</ul>
<div class="tab-content">
    <?php foreach ($pages as $key => $page) : ?>
        <div role="tabpanel" class="tab-pane <?= $key===0 ? 'active' : '' ?>" id="<?= $page->id?>">
            <?php $this->widget('application.modules.page.widgets.PagesWidget',['parent_id' => $page->id, 'view' => 'tab-content-park','limit' => 10]); ?>
        </div>
    <?php endforeach ?>
</div>
