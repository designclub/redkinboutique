            <div class="slide">
               <?php foreach ($pages as $key => $page): ?>
                 <div class="items">
                         <div class="img">
                         <?= CHtml::image($page->getImageUrl(0,0,true,null,"icon")) ?>
                     </div>
                     <div class="name">
                         <a href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]) ?>"><?= $page->title?></a>
                     </div>
                     <div class="short_content">
                         <?= $page->short_content ?>
                     </div>
                 </div>
                <?php endforeach ?>

           </div>
<?php
Yii::app()->clientScript->registerScript("slick-tab", "
   $('.slide').slick({
    autoplay: false,
    arrows: true,
    ifinity:true,
    dots:true,
    slidesToShow: 3,
    autoplaySpeed:5000,
    // appendDots:'.dots_container',
    responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        arrows: false,
                        dots:false
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1,
                        arrows: false,
                        dots:false
                    }
                },
            ]
    });
");
?>

