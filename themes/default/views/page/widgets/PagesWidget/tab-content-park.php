            <div class="autopark">
               <?php foreach ($pages as $key => $page): ?>
                 <div class="items">
                         <div class="img">
                         <?= CHtml::image($page->getImageUrl(365,248,true,null,"icon")) ?>
                     </div>
                     <div class="name">
                         <a href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]) ?>"><?= $page->title?></a>
                     </div>
                     <div class="short_content">
                         <?= $page->short_content ?>
                     </div>
                 </div>
                <?php endforeach ?>

            </div>

