<div class="top_pages">
  <?php foreach ($pages as $key => $page) : ?>
      <div class="top_page" style="position: relative">
            <div class="name">
                  <div class="title">
                     <?= $page->title ?>
                  </div>
                  <div class="link">
                     <a href="uslugi">Подробнее</a>
                  </div>
            </div>
          <div class="foto" style="position: absolute">
            <?= CHtml::image($page->getImageUrl(0,0,true,null,"icon")) ?>
          </div>
      </div>
    <?php endforeach;?>
</div>
