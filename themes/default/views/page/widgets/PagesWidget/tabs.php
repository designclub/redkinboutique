<?php /*
'promo' => 'Описание3',
'previewtext' => 'Описание2',
'year' => 'Описание1',
 */?>
 <ul class="nav nav-tabs" role="tablist">
    <li class="page_title"><label>Автопарк</label>
    <span>Более 230 едениц техники.</span>
    </li>

<?php foreach ($pages as $key => $page) : ?>
    <li role="presentation" class="<?= $key===0 ? 'active' : '' ?>">
         <a href="#<?= $page->id?>" aria-controls="home" role="tab" data-toggle="tab">
         <?= $page->title ?>
         </a>
   </li>
   <?php Yii::app()->clientScript->registerScript('tabs-'.$key, "
        $('a[href=\"#{$page->id}\"]').on('shown.bs.tab', function (e) {
            $('.slide').slick('refresh');
        });
   ") ?>
<?php endforeach;?>
</ul>
<div class="tab-content">
    <?php foreach ($pages as $key => $page) : ?>
        <div role="tabpanel" class="tab-pane <?= $key===0 ? 'active' : '' ?>" id="<?= $page->id?>">
            <?php $this->widget('application.modules.page.widgets.PagesWidget',['parent_id' => $page->id, 'view' => 'tab-content','limit' => 10]); ?>
        </div>
    <?php endforeach ?>
     <div class="tarifs">
        <a href="tarify-perevozok">Тарифная сетка</a>
     </div>
     <div class="autopark_link_wrap">
     <a href="avtopark" class="autopark_link">Весь автопарк</a>
     </div>
</div>
