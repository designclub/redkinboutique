<div id="callback" class="modal modal-my fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div data-dismiss="modal" class="modal-close"><div></div></div>
                <div class="modal-header__heading">
                    <div class="modal-header__h2">
                        Закзать звонок
                    </div>
                </div>
            </div>
                <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
                    'id'=>'callback-form-modal',
                    'type' => 'vertical',
                    'htmlOptions' => ['class' => 'form-my', 'data-type' => 'ajax-form'],
                ]); ?>

                <?php if (Yii::app()->user->hasFlash('callback-success')) : ?>
                    <script>
                        $('#callbackModal').modal('hide');
                        $('#messageModal').modal('show');
                        setTimeout(function(){
                            $('#messageModal').modal('hide');
                        }, 4000);
                    </script>
                <?php endif ?>

                <div class="modal-body">
                    <?= $form->textFieldGroup($model, 'name', [
                        'widgetOptions'=>[
                            'htmlOptions'=>[
                                'class' => '',
                                'autocomplete' => 'off'
                            ]
                        ]
                    ]); ?>
                    <div class="form-group">
                        <?= $form->labelEx($model, 'phone', ['class' => 'control-label']) ?>
                        <?php $this->widget('CMaskedTextFieldPhone', [
                            'model' => $model,
                            'attribute' => 'phone',
                            'mask' => '+7(999)999-99-99',
                            'htmlOptions'=>[
                                'class' => 'data-mask form-control',
                                'data-mask' => 'phone',
                                'placeholder' => 'Телефон',
                                'autocomplete' => 'off'
                            ]
                        ]) ?>
                        <?php echo $form->error($model, 'phone'); ?>
                    </div>

                    <?= $form->hiddenField($model, 'verify'); ?>
                    <div class="form-bot">
                        <div class="form-captcha">
                            <div class="g-recaptcha" data-sitekey="<?= Yii::app()->params['key']; ?>">
                            </div>
                            <?= $form->error($model, 'verifyCode');?>
                        </div>
                        <div class="form-button">
                            <button class="but but-lg" id="callback-modal-button" data-send="ajax">Отправить</button>
                        </div>
                    </div>
                    <div class="terms_of_use"> * Оставляя заявку Вы соглашаетесь с Условиями обработки персональных данных</div>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>