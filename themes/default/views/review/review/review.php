
<?php

$this->title = "Отзывы";
$this->description =Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = Yii::app()->getModule('yupe')->siteKeyWords;
// $this->breadcrumbs = $this->getBreadCrumbs();
$this->breadcrumbs = ["Отзывы"];
require 'phpQuery.php';
?>

<div class="container review-container">
      <h1 class="page_title"><?= $this->title ?></h1>

		<?php $this->widget(
		    'bootstrap.widgets.TbListView',
		    [
		        'dataProvider' => $dataProvider,
		        'emptyText' => '',
		        'id' => 'review-box',
		        'itemView' => '_item',
		        'summaryText' => '',
		        'template'=>'{items} {pager}',
		        'itemsCssClass' => 'review-page',
		        'ajaxUpdate'=>'true',
		        'pagerCssClass' => 'pagination-box',
		        'pager' => [
		            'header' => '',
		            'lastPageLabel' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
		            'firstPageLabel' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
		            'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
		            'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
		            'maxButtonCount' => 5,
		            'htmlOptions' => [
		                'class' => 'pagination'
		            ],
		        ]
		    ]
		); ?>

</div>

<div class="review-form_wrap">
	<div class="container">
		<div class="review-form">
					<h2 class="page_title">Оставить отзыв</h2>
					<?php $this->widget('application.modules.review.widgets.ReviewWidget',['view' => 'reviewformwidget']); ?>
		</div>
	</div>
</div>
