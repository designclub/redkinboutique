<div class="review-page__items">

        <div class="review-page__info">
            <div class="review-page__name">
                <span class="name"><?php echo CHtml::encode( $data->username); ?></span>
                <span class="date"><?= str_replace('-','.',$data->date_created); ?></span>

            </div>
            <div class="review-page__text">
                <?= $data->getShortText(200); ?>
            </div>
        </div>
            <a href="/review" class="review-page__link">Читать весь отзыв</a>

</div>
