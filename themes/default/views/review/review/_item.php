<div class="review__items">

            <div class="review__name">
                <span class="name"><?php echo CHtml::encode( $data->username); ?></span>
                <span class="date"><?= str_replace('-','.',$data->date_created); ?></span>

            </div>
            <div class="review-page__text">
                <?= $data->getShortText(200); ?>
            </div>


</div>
