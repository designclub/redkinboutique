<?php
    Yii::app()->user->returnUrl = Yii::app()->request->requestUri;
    $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        [
            'action'      => Yii::app()->createUrl('/review/create/'),
            'id'          => 'review-form',
            'type'        => 'vertical',
            'htmlOptions' => [
                'class' => '',
                'data-type' => 'ajax-form',
                'enctype' => 'multipart/form-data'
            ],
        ]
    ); ?>
    <div class="row-input">

            <?= $form->textFieldGroup($model, 'username', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'data-original-title' => $model->getAttributeLabel('username'),
                        'data-content'        => $model->getAttributeDescription('username'),
                        'autocomplete' => 'off'
                    ],
                ],
            ]); ?>


            <?= $form->textFieldGroup($model, 'useremail', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'data-original-title' => $model->getAttributeLabel('useremail'),
                        'data-content'        => $model->getAttributeDescription('useremail'),
                        'autocomplete' => 'off'
                    ],
                ],
            ]); ?>

    </div>
<div class="row-textarea">
    <?= $form->textAreaGroup($model, 'text', [
        'widgetOptions' => [
            'htmlOptions' => [
                'data-original-title' => $model->getAttributeLabel('text'),
                'data-content'        => $model->getAttributeDescription('text'),
                'autocomplete' => 'off'
            ],
        ],
    ]); ?>


    <?php
      Yii::app()->clientScript->registerScript("fileUpload", "

            $('.file-upload input[type=file]').change(function(){
                var inputFile = document.getElementById('Review_image').files;
                if(inputFile.length > 0){
                    $('#count_file').text('Выбрано файлов ' + inputFile.length);
                }else{
                    $('#count_file').text('Прикрепить фото');
                }
            });

      ");
     ?>

    <div class="form-bot">

        <div class="form-captcha">
            <div class="g-recaptcha" data-sitekey="<?= Yii::app()->params['key'] ?>">
            </div>
            <?= $form->error($model, 'verifyCode');?>
        </div>
        <div class="form-button">
            <?= CHtml::submitButton('Отправить', [
                'id' => 'reviewZayavka-button',
                'class' => 'but but-black',
                 'data-send'=>'ajax'
            ]) ?>
        </div>
    </div>
    </div>

<?php if (Yii::app()->user->hasFlash('review-success')): ?>
    <div id="messageModal" class="modal fade in" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Закрыть">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Уведомление!</h4>
                </div>
                <div class="modal-body">
                    <?= Yii::app()->user->getFlash('review-success') ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#messageModal').modal('show');
        setTimeout(function(){
            $('#messageModal').modal('hide');
        }, 5000);
    </script>
<?php endif ?>
<?php $this->endWidget(); ?>




