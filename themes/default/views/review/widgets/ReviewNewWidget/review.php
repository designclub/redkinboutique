<div class="review-product">
    <div class="review-product__header ">
    <h2>Отзывы</h2>
        <a class="review-product__link" href="/review">
            <span>Все отзывы</span>
        <i class="fa fa-chevron-right" aria-hidden="true"></i>
        </a>
    </div>
    <div class="review-page">
        <?php foreach ($model as $key => $item) : ?>
            <?php Yii::app()->controller->renderPartial('//review/review/_view', ['data' => $item]) ?>
        <?php endforeach; ?>
    </div>
    <button type="button" class="but but-black" data-toggle="modal" data-target="#reviewZayavkaModal">
                        Оставить отзыв
            </button>
</div>
