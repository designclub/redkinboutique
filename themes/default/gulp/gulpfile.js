var syntax        = 'sass', // выберете используемый синтаксис sass или scss, и перенастройте нужные пути в файле gulp.js и папки в вашего шаблоне
		gulpversion   = '4'; // Выберете обязателньо свою версию Gulp: 3 или 4

var gulp          = require('gulp'),
    autoprefixer  = require('gulp-autoprefixer'),
    browsersync   = require('browser-sync'),
    concat        = require('gulp-concat'),
    cache         = require('gulp-cache'),
    purge = require('gulp-css-purge'),
		imagemin      = require('gulp-imagemin'),
		notify        = require('gulp-notify'),
		pngquant      = require('imagemin-pngquant'),
		gutil         = require('gulp-util' ),
		rename        = require('gulp-rename'),
		rsync         = require('gulp-rsync'),
		sass          = require('gulp-sass'),
        sourcemaps = require('gulp-sourcemaps'),
		uglify        = require('gulp-uglify');


// Незабываем менять 'bul.loc' на свой локальный домен
gulp.task('browser-sync', function() {
	browsersync({
		proxy: "redkin.loc",
		notify: false,
	})
});


// Обьединяем файлы sass, сжимаем и переменовываем
gulp.task('styles', function() {
	return gulp.src('sass/**/*.scss')
	.pipe(sourcemaps.init())
	.pipe(sass({ outputStyle: 'expand' }).on("error", notify.onError()))
	//.pipe(rename({ suffix: '.min', prefix : '' }))
	.pipe(concat('style.min.css'))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(purge({
            trim : true,
            shorten : true,
            verbose : true
        })) // Opt., comment out when debugging
	.pipe(sourcemaps.write('.'))
	.pipe(gulp.dest('../web/css'))
	.pipe(browsersync.stream())

});
gulp.task('images', () => {
  return gulp.src('images/**/*')
    .pipe(cache(imagemin({verbose: true})))
    .pipe(gulp.dest('../web/images'));
});


// Обьединяем файлы скриптов, сжимаем и переменовываем
gulp.task('scripts', function() {
	return gulp.src([
		'js/main.js',
		])
	.pipe(concat('scripts.min.js'))
	.pipe(uglify()) // Mifify js (opt.)
	.pipe(gulp.dest('../web/js/'))
	.pipe(browsersync.reload({ stream: true }))

});


	gulp.task('watch', function() {
		gulp.watch('images/**/*', gulp.parallel('images')); // Наблюдение за sass файлами в папке sass в теме
		gulp.watch('sass/**/*.scss', gulp.parallel('styles')); // Наблюдение за sass файлами в папке sass в теме
		gulp.watch(['js/libs/**/*.js', 'js/main.js'], gulp.parallel('scripts')); // Наблюдение за JS файлами в папке js
    gulp.watch('./../views/**/*.php', browsersync.reload) // Наблюдение за sass файлами php в теме

	});
	gulp.task('default', gulp.parallel('styles', 'scripts', 'browser-sync', 'watch'));

