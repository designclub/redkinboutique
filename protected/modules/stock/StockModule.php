<?php
/**
 * StockModule основной класс модуля stock
 *
 * @author yupe team <team@yupe.ru>
 * @link https://yupe.ru
 * @copyright 2009-2019 amyLabs && Yupe! team
 * @package yupe.modules.stock
 * @since 0.1
 */

class StockModule  extends yupe\components\WebModule
{
    const VERSION = '0.9.8';

    /**
     * @var string
     */
    public $uploadPath = 'stock';
    /**
     * @var string
     */
    public $allowedExtensions = 'jpg,jpeg,png,gif';
    /**
     * @var int
     */
    public $minSize = 0;
    /**
     * @var int
     */
    public $maxSize = 5368709120;
    /**
     * @var int
     */
    public $maxFiles = 1;
    public $perPage = 10;

    public $metaTitle;
    /**
     * @var
     */
    public $metaDescription;
    /**
     * @var
     */
    public $metaKeyWords;
    /**
     * Массив с именами модулей, от которых зависит работа данного модуля
     *
     * @return array
     */
    public function getDependencies()
    {
        return parent::getDependencies();
    }

    /**
     * Работоспособность модуля может зависеть от разных факторов: версия php, версия Yii, наличие определенных модулей и т.д.
     * В этом методе необходимо выполнить все проверки.
     *
     * @return array или false
     */
    public function checkSelf()
    {
        return parent::checkSelf();
    }

    /**
     * Каждый модуль должен принадлежать одной категории, именно по категориям делятся модули в панели управления
     *
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('StockModule.stock', 'Сервисы');
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'perPage' => Yii::t('StockModule.stock', 'Stock per page'),
            'metaTitle' => Yii::t('StockModule.stock', 'Title tag for the news section'),
            'metaDescription' => Yii::t('StockModule.stock', 'Description for the news section'),
            'metaKeyWords' => Yii::t('StockModule.stock', 'KeyWords for the news section'),

        ];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [
            'perPage',
            'metaTitle',
            'metaDescription',
            'metaKeyWords',
        ];
    }

    /**
     * массив групп параметров модуля, для группировки параметров на странице настроек
     *
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return [
            'seo' => [
                'label' => Yii::t('StockModule.stock', 'SEO'),
                'items' => [
                    'metaTitle',
                    'metaDescription',
                    'metaKeyWords',
                ],
            ],
            'list' => [
                'label' => Yii::t('StockModule.stock', 'Stock list'),
                'items' => [
                    'perPage',
                ],
            ],
        ];
    }

    /**
     * если модуль должен добавить несколько ссылок в панель управления - укажите массив
     *
     * @return array
     */
    public function getNavigation()
    {
        return [
            ['label' => Yii::t('StockModule.stock', 'Акции')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('StockModule.stock', 'Акции'),
                'url' => ['/stock/stockBackend/index']
            ],
        ];
    }

    /**
     * текущая версия модуля
     *
     * @return string
     */
    public function getVersion()
    {
        return Yii::t('StockModule.stock', self::VERSION);
    }

    /**
     * веб-сайт разработчика модуля или страничка самого модуля
     *
     * @return string
     */
    public function getUrl()
    {
        return Yii::t('StockModule.stock', 'https://yupe.ru');
    }

    /**
     * Возвращает название модуля
     *
     * @return string.
     */
    public function getName()
    {
        return Yii::t('StockModule.stock', 'Акции');
    }

    /**
     * Возвращает описание модуля
     *
     * @return string.
     */
    public function getDescription()
    {
        return Yii::t('StockModule.stock', 'Описание модуля "stock"');
    }

    /**
     * Имя автора модуля
     *
     * @return string
     */
    public function getAuthor()
    {
        return Yii::t('StockModule.stock', 'yupe team');
    }

    /**
     * Контактный email автора модуля
     *
     * @return string
     */
    public function getAuthorEmail()
    {
        return Yii::t('StockModule.stock', 'team@yupe.ru');
    }

    /**
     * Ссылка, которая будет отображена в панели управления
     * Как правило, ведет на страничку для администрирования модуля
     *
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/stock/stockBackend/index';
    }

    /**
     * Название иконки для меню админки, например 'user'
     *
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-rub";
    }

    /**
      * Возвращаем статус, устанавливать ли галку для установки модуля в инсталяторе по умолчанию:
      *
      * @return bool
      **/
    public function getIsInstallDefault()
    {
        return parent::getIsInstallDefault();
    }

    /**
     * Инициализация модуля, считывание настроек из базы данных и их кэширование
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'stock.models.*',
                'stock.components.*',
            ]
        );
    }

    /**
     * Массив правил модуля
     * @return array
     */
    public function getAuthItems()
    {
        return [
            [
                'name' => 'Stock.StockManager',
                'description' => Yii::t('StockModule.stock', 'Manage stock'),
                'type' => AuthItem::TYPE_TASK,
                'items' => [
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Stock.StockBackend.Index',
                        'description' => Yii::t('StockModule.stock', 'Index')
                    ],
                ]
            ]
        ];
    }
}
