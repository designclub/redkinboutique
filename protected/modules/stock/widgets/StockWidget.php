<?php

/**
 * Виджет вывода последних акций
 * **/
Yii::import('application.modules.stock.models.*');

class StockWidget extends yupe\widgets\YWidget
{
    /** @var $categories mixed Список категорий, из которых выбирать новости. NULL - все */

    public $view = 'view';
    
    public $category_id;
    public $limit = 15;

    public $all = false;

    public function init()
    {

        parent::init();
    }

    public function run()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'date DESC, id DESC';
        $criteria->limit = $this->limit;
        $criteria->compare('category_id', $this->category_id);

        if($this->all) {
            $dataProvider = new CActiveDataProvider('Stock', [
                'criteria' => $criteria,
                'pagination' => [
                    'pageSize' => $this->limit
                ]
            ]);
            $this->render($this->view, ['dataProvider' => $dataProvider]);
        } else{
            $stock = Stock::model()->published()->findAll($criteria);
            $this->render($this->view, ['models' => $stock]);
        }
    }
}
