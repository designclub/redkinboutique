<?php
/**
 * Файл настроек для модуля stock
 *
 * @author yupe team <team@yupe.ru>
 * @link https://yupe.ru
 * @copyright 2009-2019 amyLabs && Yupe! team
 * @package yupe.modules.stock.install
 * @since 0.1
 *
 */
return [
    'module'    => [
        'class' => 'application.modules.stock.StockModule',
    ],
    'import'    => [],
    'component' => [],
    'rules'     => [
        '/stock/' => 'stock/stock/index',
        '/stock/categories' => 'stock/stockCategory/index',
        [
            'stock/stock/view',
            'pattern' => '/stock/<slug>',
            'urlSuffix' => '.html'
        ],
        '/stock/<slug>' => 'stock/stockCategory/view',
    ],
];