<?php

class m000000_000001_stock_add_column_cat extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{stock}}', 'name_before', 'string');
	}

	public function safeDown()
	{
        $this->dropColumn('{{stock}}', 'name_before');
	}
}