<?php
/**
 * Stock install migration
 * Класс миграций для модуля Stock:
 *
 * @category YupeMigration
 * @package  yupe.modules.stock.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     https://yupe.ru
 **/
class m000000_000000_stock_base extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->createTable(
            '{{stock}}',
            [
                'id'                => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_user_id'    => "integer NOT NULL",
                'update_user_id'    => "integer NOT NULL",
                'create_time'       => 'datetime NOT NULL',
                'update_time'       => 'datetime NOT NULL',
                'date'              => 'date NOT NULL',
                'name'              => 'string COMMENT "Название"',
                'name_short'        => 'string COMMENT "Короткое название"',
                'slug'              => 'varchar(150) NOT NULL',
                'image'             => 'string COMMENT "Изображение"',
                'description'       => 'text COMMENT "Описание"',
                'description_short' => 'text COMMENT "Краткое описание"',
                'status'            => 'integer COMMENT "Статус"',
                'position'          => 'integer COMMENT "Сортировка"',
                'meta_title'     => 'varchar(250) NOT NULL',
                'meta_keywords'     => 'varchar(250) NOT NULL',
                'meta_description'  => 'varchar(250) NOT NULL',

            ],
            $this->getOptions()
        );
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{stock}}');
    }
}
