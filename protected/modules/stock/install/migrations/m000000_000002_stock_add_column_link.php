<?php

class m000000_000002_stock_add_column_link extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{stock}}', 'button_name', 'string');
        $this->addColumn('{{stock}}', 'button_link', 'string');
	}

	public function safeDown()
	{
        $this->dropColumn('{{stock}}', 'button_name');
        $this->dropColumn('{{stock}}', 'button_link');
	}
}