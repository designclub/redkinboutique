<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yiic message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE, this file must be saved in UTF-8 encoding.
 */
return [
	'--choose--' => '--выберите--',
	'Bad characters in {attribute} field' => 'Запрещенные символы в поле {attribute}',
	'General' => 'Общие данные',
    'SEO' => 'Настройки SEO',
    'Stock per page' => 'Акций на странице',
    'Stock list' => 'Акции',
    'Stock lists' => 'Акции',
	'Title tag for the news section' => 'Тег title для раздела родительского раздела акций (/stock)',
    'Description for the news section' => 'Тег description для раздела родительского раздела акций (/stock)',
    'KeyWords for the news section' => 'Тег keywords для раздела родительского раздела акций (/stock)',
];