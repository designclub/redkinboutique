<?php
/**
* Отображение для stock/index
*
* @category YupeView
* @package  yupe
* @author   Yupe Team <team@yupe.ru>
* @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
* @link     https://yupe.ru
**/
$this->pageTitle = Yii::t('StockModule.stock', 'stock');
$this->description = Yii::t('StockModule.stock', 'stock');
$this->keywords = Yii::t('StockModule.stock', 'stock');

$this->breadcrumbs = [Yii::t('StockModule.stock', 'stock')];
?>

<h1>
    <small>
        <?php echo Yii::t('StockModule.stock', 'stock'); ?>
    </small>
</h1>