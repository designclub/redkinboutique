<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('StockModule.stock', 'Акции') => ['/stock/stockBackend/index'],
    Yii::t('StockModule.stock', 'Добавление'),
];

$this->pageTitle = Yii::t('StockModule.stock', 'Акции - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('StockModule.stock', 'Управление Акциями'), 'url' => ['/stock/stockBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('StockModule.stock', 'Добавить Акцию'), 'url' => ['/stock/stockBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('StockModule.stock', 'Акции'); ?>
        <small><?=  Yii::t('StockModule.stock', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>