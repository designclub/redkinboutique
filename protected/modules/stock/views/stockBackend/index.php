<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('StockModule.stock', 'Акции') => ['/stock/stockBackend/index'],
    Yii::t('StockModule.stock', 'Управление'),
];

$this->pageTitle = Yii::t('StockModule.stock', 'Акции - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('StockModule.stock', 'Управление Акциями'), 'url' => ['/stock/stockBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('StockModule.stock', 'Добавить Акцию'), 'url' => ['/stock/stockBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('StockModule.stock', 'Акции'); ?>
        <small><?=  Yii::t('StockModule.stock', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?=  Yii::t('StockModule.stock', 'Поиск Акций');?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
        <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('stock-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
?>
</div>

<br/>

<p> <?=  Yii::t('StockModule.stock', 'В данном разделе представлены средства управления Акциями'); ?>
</p>

<?php
 $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'stock-grid',
        'sortableRows'      => true,
        'sortableAjaxSave'  => true,
        'sortableAttribute' => 'position',
        'sortableAction'    => '/stock/stockBackend/sortable',
        'type'         => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            'id',
            [
                'name' => 'image',
                'type' => 'raw',
                'value' => function ($model) {
                    return CHtml::image($model->getImageUrl(), $model->image, ["width" => 200, "height" => 200, "class" => "img-thumbnail"]);
                },
            ],
            [
                'name' => 'name',
                'type' => 'raw',
                'value' => function ($data) {
                        return CHtml::link(\yupe\helpers\YText::wordLimiter($data->name, 5), ["/stock/stockBackend/update", "id" => $data->id]);
                    },
            ],
            'name_short',
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/stock/stockBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    Stock::STATUS_PUBLIC => ['class' => 'label-success'],
                    Stock::STATUS_MODERATE => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
