<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('StockModule.stock', 'Акции') => ['/stock/stockBackend/index'],
    $model->name,
];

$this->pageTitle = Yii::t('StockModule.stock', 'Акции - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('StockModule.stock', 'Управление Акциями'), 'url' => ['/stock/stockBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('StockModule.stock', 'Добавить Акцию'), 'url' => ['/stock/stockBackend/create']],
    ['label' => Yii::t('StockModule.stock', 'Акция') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('StockModule.stock', 'Редактирование Акции'), 'url' => [
        '/stock/stockBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('StockModule.stock', 'Просмотреть Акцию'), 'url' => [
        '/stock/stockBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('StockModule.stock', 'Удалить Акцию'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/stock/stockBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('StockModule.stock', 'Вы уверены, что хотите удалить Акцию?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('StockModule.stock', 'Просмотр') . ' ' . Yii::t('StockModule.stock', 'Акции'); ?>        <br/>
        <small>&laquo;<?=  $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data'       => $model,
    'attributes' => [
        'id',
        'create_user_id',
        'update_user_id',
        'create_time',
        'update_time',
        'date',
        'name',
        'name_short',
        'alias',
        'image',
        'description',
        'description_short',
        'status',
        'position',
        'meta_title',
        'meta_keywords',
        'meta_description',
    ],
]); ?>
