<?php
/**
* StockController контроллер для stock на публичной части сайта
*
* @author yupe team <team@yupe.ru>
* @link https://yupe.ru
* @copyright 2009-2019 amyLabs && Yupe! team
* @package yupe.modules.stock.controllers
* @since 0.1
*
*/

class StockController extends \yupe\components\controllers\FrontController
{
    /**
     * @param $slug
     * @throws CHttpException
     */
    public function actionView($slug)
    {
        $model = Stock::model()->published();

        if (!$model) {
            throw new CHttpException(404, Yii::t('NewsModule.news', 'News article was not found!'));
        }

        $this->render('view', ['model' => $model]);
    }

    /**
     *
     */
    public function actionIndex()
    {
        $dbCriteria = new CDbCriteria([
            'condition' => 't.status = :status',
            'params' => [
                ':status' => Stock::STATUS_PUBLIC,
            ],
            'order' => 't.date DESC',
            'with' => ['user'],
        ]);
        
        $dataProvider = new CActiveDataProvider('Stock', [
            'criteria' => $dbCriteria,
            'pagination' => [
                'pageSize' => (int)$this->getModule()->perPage,
            ],
        ]);

        $this->render('index', ['dataProvider' => $dataProvider]);
    }
}