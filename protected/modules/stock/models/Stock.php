<?php

/**
 * This is the model class for table "{{stock}}".
 *
 * The followings are the available columns in table '{{stock}}':
 * @property integer $id
 * @property integer $create_user_id
 * @property integer $update_user_id
 * @property string $create_time
 * @property string $update_time
 * @property string $date
 * @property string $name
 * @property string $name_short
 * @property string $slug
 * @property string $image
 * @property string $description
 * @property string $description_short
 * @property integer $status
 * @property integer $position
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $name_before
 * @property string $button_name
 * @property string $button_link
 */
class Stock extends yupe\models\YModel
{
	const STATUS_PUBLIC = 1;
	const STATUS_MODERATE = 0;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{stock}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			['name, name_short, slug', 'required'],
			['create_user_id, update_user_id, status, position', 'numerical', 'integerOnly'=>true],
			['name, name_short, name_before, button_name, button_link, image, slug', 'length', 'max'=>255],
			['meta_title, meta_keywords, meta_description', 'length', 'max'=>250],
			[
                'slug',
                'yupe\components\validators\YSLugValidator',
                'message' => Yii::t('StockModule.stock', 'Bad characters in {attribute} field')
            ],
			['description, description_short,date, button_name, button_link', 'safe'],
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			['id, create_user_id, update_user_id, create_time, update_time, date, name, name_short, name_before, slug, image, description, description_short, status, position, meta_title, meta_keywords, meta_description, button_name, button_link', 'safe', 'on'=>'search'],
		];
	}

	public function behaviors()
	{
	    $module = Yii::app()->getModule('stock');

	    return [
	        'imageUpload' => [
	            'class'         => 'yupe\components\behaviors\ImageUploadBehavior',
	            'attributeName' => 'image',
	            'minSize'       => $module->minSize,
	            'maxSize'       => $module->maxSize,
	            'types'         => $module->allowedExtensions,
	            'uploadPath'    => $module->uploadPath,
	        ],
	        'sortable' => [
	            'class' => 'yupe\components\behaviors\SortableBehavior',
	        ],
	    ];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'create_user_id' => 'Create User',
			'update_user_id' => 'Update User',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'date' => 'Date',
			'name' => 'Название',
			'name_short' => 'Короткое название',
			'slug' => 'slug',
			'image' => 'Изображение',
			'description_short' => 'Короткое описание',
			'description' => 'Описание',
			'status' => 'Статус',
			'position' => 'Сортировка',
			'meta_title' => 'Meta Title',
			'meta_keywords' => 'Meta Keywords',
			'meta_description' => 'Meta Description',
			'name_before' => 'Текст перед названием',
			'button_name' => 'Название кнопки',
			'button_link' => 'Ссылка для кнопки',
		);
	}
	/**
     * @return bool
     */
    public function beforeSave()
    {
        $this->update_time = new CDbExpression('NOW()');
        $this->update_user_id = Yii::app()->getUser()->getId();
        $this->date = date('Y-m-d', strtotime($this->date));

        if ($this->getIsNewRecord()) {
            $this->create_time = $this->update_time;
            $this->create_user_id = $this->update_user_id;
        }

        return parent::beforeSave();
    }

    public function afterFind()
    {
        $this->date = date('d-m-Y', strtotime($this->date));

        return parent::afterFind();
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('update_user_id',$this->update_user_id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		if ($this->date) {
            $criteria->compare('date', date('Y-m-d', strtotime($this->date)));
        }
		$criteria->compare('name',$this->name,true);
		$criteria->compare('name_short',$this->name_short,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('description_short',$this->description_short,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('position',$this->position);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('name_before',$this->name_before);
		$criteria->compare('button_name',$this->button_name);
		$criteria->compare('button_link',$this->button_link);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => ['defaultOrder' => 't.position'],
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Stock the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getStatusList()
	{
		return [
			self::STATUS_PUBLIC   => 'Опубликован',
			self::STATUS_MODERATE => 'На модерации',
		];
	}

	public function getStatusName()
	{
		$data = $this->getStatusList();
		if (isset($data[$this->status])) {
			return $data[$this->status];
		}
		return null;
	}

	public function scopes()
    {
        return [
            'published' => [
                'condition' => 'status  = :status',
                'params' => [
                    ':status' => self::STATUS_PUBLIC
                ]
            ],
        ];
    }
}
