<?php
/**
 * FormQuestionsWidget виджет формы "остались вопросы"
 */
Yii::import('application.modules.mail.models.form.WriteToUsForm');

class WriteToUsWidget extends yupe\widgets\YWidget
{
    public $view = 'writetous-widget';

    public function run()
    {
        $model = new WriteToUsForm;
        if (isset($_POST['WriteToUsForm'])) {
            $model->attributes = $_POST['WriteToUsForm'];
            if($model->verify == ''){
                if ($model->validate()) {
                    Yii::app()->user->setFlash('writetous-success', 'Ваша заявка успешно отправлена.');
                    Yii::app()->controller->refresh();
                }
            }
        }      

        $this->render($this->view, [
            'model' => $model,
        ]);
    }
}
