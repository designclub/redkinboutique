<?php
/**
 * CareesWidget виджет формы "Карьера связь с нами"
 */
Yii::import('application.modules.mail.models.form.CallbackFormModal');

class CallbackModalWidget extends yupe\widgets\YWidget
{
    public $view = 'callback-widget';

    public function run()
    {
        $model = new CallbackFormModal;
        if (isset($_POST['CallbackFormModal'])) {
            $model->attributes = $_POST['CallbackFormModal'];
            if($model->verify == ''){
                if ($model->validate()) {
                    Yii::app()->user->setFlash('callback-success', Yii::t('MailModule.mail', 'Ваша заявка успешно отправлена.'));
                    Yii::app()->controller->refresh();
                }
            }
        }      

        $this->render($this->view, [
            'model' => $model,
        ]);
    }

}
