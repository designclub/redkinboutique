<?php
/**
 * ReviewsWidget виджет для вывода страниц
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.review.widgets
 * @since 0.1
 *
 */
Yii::import('application.modules.review.models.*');

class ReviewWidget extends yupe\widgets\YWidget
{
    public $view = '';

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $model = new Review();
        $module = Yii::app()->getModule('review');
        if (isset($_POST['Review'])) {
            $model->attributes   = $_POST['Review'];
            $model->date_created =  date("Y-m-d H:i:s");

            if ($module->moderation == 1) {
                $model->moderation = 2;
            } else {
                $model->moderation = 1;
            }

            if ($model->save()) {
                $model->notification($module->email_notification);
                Yii::app()->user->setFlash('review-success', 'Спасибо за Ваш отзыв, после прохождения модерации,он будет доступен для просмотра!');
                Yii::app()->controller->refresh();
            }
        }
        $this->render($this->view, [
            'model' => $model,
        ]);

       /* $model = new Review;

        $this->render($this->view, ['model' => $model]);*/
    }
}
