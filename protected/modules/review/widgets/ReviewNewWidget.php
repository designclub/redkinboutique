<?php
/**
 * ReviewNewWidget виджет для вывода страниц
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.review.widgets
 * @since 0.1
 *
 */
Yii::import('application.modules.review.models.*');

class ReviewNewWidget extends yupe\widgets\YWidget
{
	public $slide = false;
  public $limit = 5;
	public $category_id;
	public $view = 'review';

    public function run()
    {
		$criteria = new CDbCriteria();

        $criteria->addCondition("t.moderation = 1");
        $criteria->order = 't.position DESC';
        $criteria->limit = $this->limit;

        if($this->category_id){
            $criteria->addCondition("t.category_id = {$this->category_id}");
        }

        if($this->slide == true){
            $this->view = 'review-carousel';
        }

        $model = Review::model()->findAll($criteria);

        $this->render($this->view, [
        	'model' => $model
        ]);
    }
}
