<?php

class m181218_121817_add_store_is extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_category}}', 'is_home', "boolean not null default '0'");
        $this->addColumn('{{store_product}}', 'is_new', "boolean not null default '0'");
        $this->addColumn('{{store_product}}', 'is_hit', "boolean not null default '0'");
        $this->addColumn('{{store_product}}', 'is_popular', "boolean not null default '0'");
        $this->addColumn('{{store_product}}', 'is_best', "boolean not null default '0'");
        $this->addColumn('{{store_product}}', 'is_top', "boolean not null default '0'");
        $this->addColumn('{{store_product}}', 'is_sale', "boolean not null default '0'");
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_category}}', 'is_home');
        $this->dropColumn('{{store_product}}', 'is_new');
        $this->dropColumn('{{store_product}}', 'is_hit');
        $this->dropColumn('{{store_product}}', 'is_popular');
        $this->dropColumn('{{store_product}}', 'is_best');
        $this->dropColumn('{{store_product}}', 'is_top');
        $this->dropColumn('{{store_product}}', 'is_sale');
    }
}