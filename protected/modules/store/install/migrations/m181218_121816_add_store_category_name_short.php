<?php

class m181218_121816_add_store_category_name_short extends yupe\components\DbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{store_category}}', 'name_short', 'varchar(250) not null');
	}

	public function safeDown()
	{
		$this->dropColumn('{{store_category}}', 'name_short');
	}
}