<?php
/**
 * PagesWidget виджет для вывода страниц
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.page.widgets
 * @since 0.1
 *
 */
Yii::import('application.modules.page.models.*');

/**
 * Class PagesNewWidget
 */
class PagesNewWidget extends yupe\widgets\YWidget
{
    public $id;
    public $parent_id;
    public $limit;
    /**
     * @var string
     */
    public $view = 'pageswidget';

    protected $pages;

    public function init()
    {
        if($this->parent_id){
            $criteria = new CDbCriteria(array(
                'condition'=>'parent_id=:parent_id',
                'params'=>array(':parent_id'=>$this->parent_id),
            ));
            $criteria->addCondition("status = 1");
            $criteria->order = 't.order ASC';
            
            if($this->limit){
                $criteria->limit = $this->limit;
            }

            $this->pages = Page::model()->findAll($criteria);
        } elseif($this->id) {
            $this->pages = Page::model()->findByPk($this->id);
        }
        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $this->render($this->view,[
            'pages' => $this->pages,
        ]);
    }
}
