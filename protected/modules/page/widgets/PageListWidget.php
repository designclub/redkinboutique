<?php
/**
 * RegionFilterWidget виджет фильтрации областей
 *
 */
Yii::import('application.modules.page.models.*');

/**
 * Class PagesWidget
 */
class PageListWidget extends yupe\widgets\YWidget
{
    /**
     * @var
     */
    public $parent_id;
    public $category_id;
    public $order = 't.order ASC, t.create_time ASC';

    public $limit;

    public $view = 'page_list';

    protected $lists;

    public function run()
    {
        $criteria = new CDbCriteria();
        $criteria->order = $this->order;
        if ($this->limit) {
                $criteria->limit = (int)$this->limit;
            }
             if ($this->category_id) {
                $criteria->addCondition("category_id = {$this->category_id}");
            }
            if ($this->parent_id) {
                $criteria->addCondition("parent_id = {$this->parent_id}");
            }
            if ($this->topLevelOnly) {
                $criteria->addCondition("parent_id is null or parent_id = 0");
            }

        $this->lists = Page::model()->findAll($criteria);

        $this->render(
            $this->view,
            [
                'lists' => $this->lists,
            ]
        );
    }
}
